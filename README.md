![](https://gitee.com/luyu-community/luyu-cross-chain-protocol/raw/master/doc/img/logo_nor.svg)

# 陆羽跨链协议 JAVA SDK

##  引用方式

### maven

以`gradle`为例

```
dependencies { implementation 'link.luyu:luyu-java-sdk:1.0.1'}
```

## 配置

### 生成账户私钥

``` bash
gradle assemble # 生成dist目录
cd dist
bash gen_account.sh
[SUCCESS] Account secret key generated: 0x073cbbe73e51720610219358cdf369b78bc49f78.key # 0xxxxx.key 即为生成的账户私钥
```

### 连接跨链路由

**拷贝证书**

[生成router网络](https://gitee.com/luyu-community/router)后，把router网络的相关证书拷贝至conf目录下

``` bash
cd dist
cp xxxx/routers/cert/sdk/* conf/
```

**配置文件**

``` bash
cp conf/application-sample.toml conf/application.toml
vim conf/application.toml
```

内容为

``` toml
[connection]
    server =  '192.168.1.1:8250' # 路由ip:port
    sslKey = 'classpath:ssl.key' 
    sslCert = 'classpath:ssl.crt'
    caCert = 'classpath:ca.crt'
    sslSwitch = 2  # SSL开关（需与Router的rpc.sslSwitch 配置保持一致），关闭:2, 单向验证（验路由证书）:1，双向验证: 0
```

## 使用方式

初始化

``` java
try {
    // 初始化账户
    LuyuAccount account = AccountFactory.build("classpath:0x073cbbe73e51720610219358cdf369b78bc49f78.key");
    // LuyuAccount account = AccountFactory.build(); // Generate temp account
    
    // 初始化 SDK
    LuyuSDK sdk = SDKFactory.build(account);
    
    // 使用

} catch (LuyuSDKException e) {
    System.out.println(e.getMessage());
}
```

查询资源举例

``` java
ListResourcesExt.Response response = sdk.listResources(false).send();
```

发送交易举例

``` java
Transaction tx = new Transaction();
tx.setPath("payment.bcos.HelloWorld");
tx.setMethod("set");
tx.setArgs(new String[] {"aaaaaaaaa"});

Receipt receipt = sdk.sendTransaction(tx).send();
```

查询状态举例

``` java
CallRequest request = new CallRequest();
request.setPath("payment.bcos.HelloWorld");
request.setMethod("get");
request.setArgs(new String[0]);

CallResponse response = sdk.call(request).send();
System.out.println(response.toString());
```

* 更多使用方法：[陆羽协议 SDK 部分](https://gitee.com/luyu-community/luyu-cross-chain-protocol/blob/develop/src/main/java/link/luyu/protocol/application/SDK.java)

* 异步接口：[RemoteCall](https://gitee.com/luyu-community/luyu-cross-chain-protocol/blob/develop/src/main/java/link/luyu/protocol/application/RemoteCall.java)



## License

开源协议为Apache License 2.0，详情参考[LICENSE](./LICENSE)。

