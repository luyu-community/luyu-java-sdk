package link.luyu.sdk;

import java.util.Arrays;
import link.luyu.protocol.algorithm.ecdsa.secp256k1.EcdsaSecp256k1WithSHA256;
import link.luyu.sdk.common.ECSecKeyPem;
import org.junit.Assert;
import org.junit.Test;

public class ECSecKeyPemTest {
    private byte[] secKey;
    private byte[] pubKey;
    private String address;
    private String pemFileName;

    public ECSecKeyPemTest() {
        this.secKey =
                new byte[] {
                    0, -23, -95, 33, -40, 89, -44, -109, -35, -110, 99, -64, 27, 123, 53, 44, 125,
                    99, -26, 67, -48, 3, 122, 76, -43, -19, -50, 87, 7, -7, 121, -117, 40
                };
        this.pubKey = EcdsaSecp256k1WithSHA256.secKey2PubKey(secKey);
        this.address = EcdsaSecp256k1WithSHA256.getAddress(pubKey);
        this.pemFileName = "0x" + address + ".key";
    }

    @Test
    public void readTest() throws Exception {
        String filePath = "classpath:" + pemFileName;
        byte[] secKeyCmp = ECSecKeyPem.read(filePath);

        Assert.assertTrue(Arrays.equals(secKeyCmp, this.secKey));
    }

    @Test
    public void writeTest() throws Exception {
        ECSecKeyPem.write("src/test/resources/" + pemFileName, secKey);
    }
}
