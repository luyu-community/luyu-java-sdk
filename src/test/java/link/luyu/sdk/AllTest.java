package link.luyu.sdk;

import link.luyu.protocol.network.CallRequest;
import link.luyu.protocol.network.CallResponse;
import link.luyu.protocol.network.LuyuAccount;
import link.luyu.protocol.network.Receipt;
import link.luyu.protocol.network.Transaction;
import link.luyu.sdk.account.AccountFactory;
import link.luyu.sdk.caller.LuyuSDK;
import link.luyu.sdk.caller.SDKFactory;
import link.luyu.sdk.caller.extend.ListResourcesExt;
import org.junit.Assert;
import org.junit.Test;

public class AllTest {
    public LuyuAccount account;
    public LuyuSDK sdk;

    public AllTest() throws Exception {
        account = AccountFactory.build();
        sdk = SDKFactory.build(account);
    }

    @Test
    public void sendTransactionTest() {
        Transaction tx = new Transaction();
        tx.setPath("payment.bcos.HelloWorld");
        tx.setMethod("set");
        tx.setArgs(new String[] {"aaaaaaaaa"});

        Receipt receipt = sdk.sendTransaction(tx).send();
        System.out.println(receipt.toString());
        Assert.assertTrue(receipt.getCode() == 0); // SUCCESS
    }

    @Test
    public void callTest() {
        CallRequest request = new CallRequest();
        request.setPath("payment.bcos.HelloWorld");
        request.setMethod("get");
        request.setArgs(new String[0]);

        CallResponse response = sdk.call(request).send();
        System.out.println(response.toString());
        Assert.assertTrue(response.getCode() == 0); // SUCCESS
    }

    @Test
    public void listResourcesTest() {
        ListResourcesExt.Response response = sdk.listResources(false).send();
        Assert.assertTrue(response != null);
        System.out.println(response.total);
    }
}
