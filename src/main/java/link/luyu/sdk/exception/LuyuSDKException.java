package link.luyu.sdk.exception;

public class LuyuSDKException extends Exception {

    private static final long serialVersionUID = 3754251347587995515L;

    private final Integer errorCode;

    public LuyuSDKException(Integer code, String message) {
        super(message);
        errorCode = code;
    }

    public Integer getErrorCode() {
        return errorCode;
    }
}
