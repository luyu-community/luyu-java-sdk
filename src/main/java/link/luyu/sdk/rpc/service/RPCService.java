package link.luyu.sdk.rpc.service;

import link.luyu.sdk.exception.LuyuSDKException;

public interface RPCService {

    void init() throws LuyuSDKException;

    <T> Response<T> send(String httpMethod, String uri, Request request) throws Exception;

    <T> void asyncSend(String httpMethod, String uri, Request<?> request, Callback<T> callback);
}
