package link.luyu.sdk.rpc.service;

import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import link.luyu.sdk.exception.ErrorCode;
import link.luyu.sdk.exception.LuyuSDKException;

public abstract class Callback<T> {
    private static Timer timer = new HashedWheelTimer();
    private static final long CALLBACK_TIMEOUT = 30000; // ms
    private Timeout timeoutWorker;
    private AtomicBoolean isFinish = new AtomicBoolean(false);

    public Callback() {
        timeoutWorker =
                timer.newTimeout(
                        timeout -> {
                            if (!isFinish.getAndSet(true)) {
                                timeoutWorker.cancel();
                                onFailed(
                                        new LuyuSDKException(
                                                ErrorCode.REMOTECALL_ERROR, "Timeout"));
                            }
                        },
                        CALLBACK_TIMEOUT,
                        TimeUnit.MILLISECONDS);
    }

    public abstract void onSuccess(Response<T> response);

    public abstract void onFailed(LuyuSDKException e);

    public void callOnSuccess(Response<T> response) {
        if (!isFinish.getAndSet(true)) {

            timeoutWorker.cancel();
            onSuccess(response);
        }
    }

    public void callOnFailed(LuyuSDKException e) {
        if (!isFinish.getAndSet(true)) {

            timeoutWorker.cancel();
            onFailed(e);
        }
    }
}
