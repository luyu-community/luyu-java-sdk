package link.luyu.sdk;

import link.luyu.protocol.network.CallRequest;
import link.luyu.protocol.network.CallResponse;
import link.luyu.protocol.network.LuyuAccount;
import link.luyu.protocol.network.Receipt;
import link.luyu.protocol.network.Transaction;
import link.luyu.sdk.account.AccountFactory;
import link.luyu.sdk.caller.LuyuSDK;
import link.luyu.sdk.caller.SDKFactory;
import link.luyu.sdk.exception.LuyuSDKException;

public class Example {
    public static void main(String[] args) {
        try {
            // LuyuAccount account =
            // AccountFactory.build("classpath:0x073cbbe73e51720610219358cdf369b78bc49f78.key");
            LuyuAccount account = AccountFactory.build(); // Generate temp account
            LuyuSDK sdk = SDKFactory.build(account);
            sdk.listResources(false);

            Transaction tx = new Transaction();
            tx.setPath("payment.bcos.HelloWorld");
            tx.setMethod("set");
            tx.setArgs(new String[] {"aaaaaaaaa"});
            Receipt receipt = sdk.sendTransaction(tx).send();
            System.out.println("Receipt: " + receipt);

            CallRequest request = new CallRequest();
            request.setPath("payment.bcos.HelloWorld");
            request.setMethod("get");
            request.setArgs(new String[0]);

            CallResponse response = sdk.call(request).send();
            System.out.println("CallResponse: " + response);

        } catch (LuyuSDKException e) {
            System.out.println(e.getMessage());
        } finally {
            System.exit(0);
        }
    }
}
