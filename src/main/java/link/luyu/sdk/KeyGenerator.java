package link.luyu.sdk;

import link.luyu.protocol.algorithm.ecdsa.secp256k1.EcdsaSecp256k1WithSHA256;
import link.luyu.sdk.common.ECSecKeyPem;

public class KeyGenerator {
    public static void main(String[] args) {
        try {
            byte[] content = EcdsaSecp256k1WithSHA256.generateKeyPairPKCS8Encoded();

            byte[] secKey = ECSecKeyPem.readContent(content);
            byte[] pubKey = EcdsaSecp256k1WithSHA256.secKey2PubKey(secKey);
            String address = EcdsaSecp256k1WithSHA256.getAddress(pubKey);

            String secKeyFile = address + ".key";
            ECSecKeyPem.writeEncoded(secKeyFile, content);
            System.out.println("[SUCCESS] Account secret key generated: " + secKeyFile);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
