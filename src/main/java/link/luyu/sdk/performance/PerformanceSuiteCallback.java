package link.luyu.sdk.performance;

public interface PerformanceSuiteCallback {
    void onSuccess(String message);

    void onFailed(String message);

    void releaseLimiter();

    void onSuccessWithoutRelease(String message);

    void onFailedWithoutRelease(String message);
}
