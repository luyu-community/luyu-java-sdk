package link.luyu.sdk.performance.BCOS;

import link.luyu.protocol.application.RemoteCall;
import link.luyu.protocol.network.CallRequest;
import link.luyu.protocol.network.CallResponse;
import link.luyu.sdk.caller.LuyuSDK;
import link.luyu.sdk.exception.LuyuSDKException;
import link.luyu.sdk.performance.PerformanceSuiteCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BCOSCallSuite extends BCOSSuite {
    private static Logger logger = LoggerFactory.getLogger(BCOSCallSuite.class);

    public BCOSCallSuite(LuyuSDK sdk) throws LuyuSDKException {
        super(sdk);
    }

    @Override
    public String getName() {
        return "BCOS Call Suite";
    }

    @Override
    public void call(PerformanceSuiteCallback callback, int index) {
        CallRequest callRequest = newCallRequest();
        getSdk().call(callRequest)
                .asyncSend(
                        new RemoteCall.Callback<CallResponse>() {
                            @Override
                            public void onResponse(
                                    int status, String message, CallResponse response) {
                                if (status == 0) {
                                    callback.onSuccess("Ok");
                                } else {
                                    callback.onFailed(message);
                                }
                            }
                        });
    }
}
