package link.luyu.sdk.performance.BCOS;

import java.math.BigInteger;
import link.luyu.protocol.network.LuyuAccount;
import link.luyu.sdk.account.AccountFactory;
import link.luyu.sdk.caller.LuyuSDK;
import link.luyu.sdk.caller.SDKFactory;
import link.luyu.sdk.performance.PerformanceManager;
import link.luyu.sdk.performance.PerformanceSuite;

public class BCOSPerformanceTest {

    public static void usage() {
        System.out.println("Usage:");
        System.out.println(
                " \t java -cp conf/:lib/*:apps/* "
                        + BCOSPerformanceTest.class.getName()
                        + " call [count] [qps] [poolSize]");
        System.out.println(
                " \t java -cp conf/:lib/*:apps/* "
                        + BCOSPerformanceTest.class.getName()
                        + " sendTransaction [count] [qps] [poolSize]");
        System.out.println("Example:");
        System.out.println(
                " \t java -cp conf/:lib/*:apps/* "
                        + BCOSPerformanceTest.class.getName()
                        + " call 100 10 2000");
        System.out.println(
                " \t java -cp conf/:lib/*:apps/* "
                        + BCOSPerformanceTest.class.getName()
                        + " sendTransaction 100 10 500");
        exit();
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 4) {
            usage();
        }

        String command = args[0];
        BigInteger count = new BigInteger(args[1]);
        BigInteger qps = new BigInteger(args[2]);
        int poolSize = Integer.parseInt(args[3]);

        System.out.println(
                "BCOSPerformanceTest: "
                        + ", command: "
                        + command
                        + ", count: "
                        + count
                        + ", qps: "
                        + qps);

        switch (command) {
            case "call":
                callTest(count, qps, poolSize);
                exit();
                break;
            case "sendTransaction":
                sendTransactionTest(count, qps, poolSize);
                exit();
                break;
            default:
                usage();
        }
    }

    public static void callTest(BigInteger count, BigInteger qps, int poolSize) {

        try {
            LuyuAccount account = AccountFactory.build();
            LuyuSDK sdk = SDKFactory.build(account);
            PerformanceSuite suite = new BCOSCallSuite(sdk);
            PerformanceManager performanceManager =
                    new PerformanceManager(suite, count, qps, poolSize);
            performanceManager.run();

        } catch (Exception e) {
            System.out.println("callTest Error: " + e.getMessage());
        }
    }

    public static void sendTransactionTest(BigInteger count, BigInteger qps, int poolSize) {
        try {
            LuyuAccount account = AccountFactory.build();
            LuyuSDK sdk = SDKFactory.build(account);
            PerformanceSuite suite = new BCOSSendTransactionSuite(sdk);
            PerformanceManager performanceManager =
                    new PerformanceManager(suite, count, qps, poolSize);
            performanceManager.run();

        } catch (Exception e) {
            System.out.println("sendTransactionTest Error: " + e.getMessage());
        }
    }

    private static void exit() {
        System.exit(0);
    }
}
