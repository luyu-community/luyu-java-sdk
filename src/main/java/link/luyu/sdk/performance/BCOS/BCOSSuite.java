package link.luyu.sdk.performance.BCOS;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import link.luyu.protocol.application.RemoteCall;
import link.luyu.protocol.network.CallRequest;
import link.luyu.protocol.network.Receipt;
import link.luyu.protocol.network.Transaction;
import link.luyu.sdk.caller.LuyuSDK;
import link.luyu.sdk.exception.ErrorCode;
import link.luyu.sdk.exception.LuyuSDKException;
import link.luyu.sdk.performance.PerformanceSuite;

public abstract class BCOSSuite implements PerformanceSuite {
    private String path = "payment.bcos.HelloWorld";
    private LuyuSDK sdk;

    public BCOSSuite(LuyuSDK sdk) throws LuyuSDKException {
        this.sdk = sdk;
        check();
    }

    public Transaction newTransaction() {
        Transaction request = new Transaction();
        request.setPath(path);
        request.setMethod("set");
        request.setArgs(new String[] {"test data"});
        return request;
    }

    public CallRequest newCallRequest() {
        CallRequest request = new CallRequest();
        request.setPath(path);
        request.setMethod("get");
        request.setArgs(new String[] {});
        return request;
    }

    private void check() throws LuyuSDKException {
        if (sdk == null) {
            throw new LuyuSDKException(ErrorCode.RESOURCE_INACTIVE, "SDK init failed.");
        }

        Transaction tx = newTransaction();
        CompletableFuture<String> future = new CompletableFuture<>();
        sdk.sendTransaction(tx)
                .asyncSend(
                        new RemoteCall.Callback<Receipt>() {
                            @Override
                            public void onResponse(int status, String message, Receipt response) {
                                if (status != 0) {
                                    future.complete(message);
                                } else {
                                    future.complete(null);
                                }
                            }
                        });

        String errorMessage;
        try {
            errorMessage = future.get(30, TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new LuyuSDKException(
                    ErrorCode.RESOURCE_INACTIVE, "Access resource: " + path + " timeout");
        }

        if (errorMessage != null) {
            throw new LuyuSDKException(
                    ErrorCode.RESOURCE_INACTIVE,
                    "Access resource " + path + " error: " + errorMessage);
        }
    }

    public LuyuSDK getSdk() {
        return sdk;
    }
}
