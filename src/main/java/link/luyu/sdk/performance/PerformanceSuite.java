package link.luyu.sdk.performance;

public interface PerformanceSuite {
    String getName();

    void call(PerformanceSuiteCallback callback, int index);
}
