package link.luyu.sdk.performance.Fabric;

import link.luyu.protocol.application.RemoteCall;
import link.luyu.protocol.network.Receipt;
import link.luyu.protocol.network.Transaction;
import link.luyu.sdk.caller.LuyuSDK;
import link.luyu.sdk.exception.LuyuSDKException;
import link.luyu.sdk.performance.PerformanceSuiteCallback;

public class FabricSendTransactionSuite extends FabricSuite {

    public FabricSendTransactionSuite(LuyuSDK sdk) throws LuyuSDKException {
        super(sdk);
    }

    @Override
    public String getName() {
        return "Fabric Send Transaction Suite";
    }

    @Override
    public void call(PerformanceSuiteCallback callback, int index) {
        Transaction transaction = newTransaction();
        getSdk().sendTransaction(transaction)
                .asyncSend(
                        new RemoteCall.Callback<Receipt>() {
                            @Override
                            public void onResponse(int status, String message, Receipt response) {
                                if (status == 0) {
                                    callback.onSuccess("Ok");
                                } else {
                                    callback.onFailed(message);
                                }
                            }
                        });
    }
}
