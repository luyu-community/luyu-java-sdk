package link.luyu.sdk.performance.Fabric;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import link.luyu.protocol.application.RemoteCall;
import link.luyu.protocol.network.CallRequest;
import link.luyu.protocol.network.Receipt;
import link.luyu.protocol.network.Transaction;
import link.luyu.sdk.caller.LuyuSDK;
import link.luyu.sdk.exception.ErrorCode;
import link.luyu.sdk.exception.LuyuSDKException;
import link.luyu.sdk.performance.PerformanceSuite;

public abstract class FabricSuite implements PerformanceSuite {
    private String path = "payment.fabric.sacc";
    private LuyuSDK sdk;

    private String deployHelp =
            "Please check router has started and deploy it by:\n"
                    + "\tdocker exec -it cli peer chaincode install -n sacc -v 1.0 -p github.com/chaincode/sacc/\n"
                    + "\tdocker exec -it cli peer chaincode instantiate -o orderer.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n sacc -l golang -v 1.0 -c '{\"Args\":[\"a\",\"10\"]}' -P 'OR ('\\''Org1MSP.peer'\\'','\\''Org2MSP.peer'\\'')'";

    public FabricSuite(LuyuSDK sdk) throws LuyuSDKException {
        this.sdk = sdk;
        check();
    }

    public Transaction newTransaction() {
        Transaction request = new Transaction();
        request.setPath(path);
        request.setMethod("set");
        request.setArgs(new String[] {"a", "1000"});
        return request;
    }

    public CallRequest newCallRequest() {
        CallRequest request = new CallRequest();
        request.setPath(path);
        request.setMethod("get");
        request.setArgs(new String[] {"a"});
        return request;
    }

    private void check() throws LuyuSDKException {
        if (sdk == null) {
            throw new LuyuSDKException(ErrorCode.RESOURCE_INACTIVE, "SDK init failed.");
        }

        Transaction tx = newTransaction();
        CompletableFuture<String> future = new CompletableFuture<>();
        sdk.sendTransaction(tx)
                .asyncSend(
                        new RemoteCall.Callback<Receipt>() {
                            @Override
                            public void onResponse(int status, String message, Receipt response) {
                                if (status != 0) {
                                    future.complete(message);
                                } else {
                                    future.complete(null);
                                }
                            }
                        });

        String errorMessage;
        try {
            errorMessage = future.get(30, TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new LuyuSDKException(
                    ErrorCode.RESOURCE_INACTIVE,
                    "Access resource: " + path + " timeout. " + deployHelp);
        }

        if (errorMessage != null) {
            throw new LuyuSDKException(
                    ErrorCode.RESOURCE_INACTIVE,
                    "Access resource " + path + " error: " + errorMessage + "\n" + deployHelp);
        }
    }

    public LuyuSDK getSdk() {
        return sdk;
    }
}
