package link.luyu.sdk.performance.Fabric;

import link.luyu.protocol.application.RemoteCall;
import link.luyu.protocol.network.CallRequest;
import link.luyu.protocol.network.CallResponse;
import link.luyu.sdk.caller.LuyuSDK;
import link.luyu.sdk.exception.LuyuSDKException;
import link.luyu.sdk.performance.PerformanceSuiteCallback;

public class FabricCallSuite extends FabricSuite {

    public FabricCallSuite(LuyuSDK sdk) throws LuyuSDKException {
        super(sdk);
    }

    @Override
    public String getName() {
        return "Fabric Call Suite";
    }

    @Override
    public void call(PerformanceSuiteCallback callback, int index) {
        CallRequest callRequest = newCallRequest();
        getSdk().call(callRequest)
                .asyncSend(
                        new RemoteCall.Callback<CallResponse>() {
                            @Override
                            public void onResponse(
                                    int status, String message, CallResponse response) {
                                if (status == 0) {
                                    callback.onSuccess("Ok");
                                } else {
                                    callback.onFailed(message);
                                }
                            }
                        });
    }
}
