package link.luyu.sdk.account;

import java.util.Map;
import link.luyu.protocol.algorithm.SignAlgManager;
import link.luyu.protocol.algorithm.SignatureAlgorithm;
import link.luyu.protocol.algorithm.ecdsa.secp256k1.EcdsaSecp256k1WithSHA256;
import link.luyu.protocol.network.ECDSALuyuSign;
import link.luyu.protocol.network.LuyuAccount;
import link.luyu.protocol.network.LuyuSign;
import link.luyu.sdk.common.ECSecKeyPem;
import link.luyu.sdk.exception.LuyuSDKException;

public class AccountFactory {
    public static LuyuAccount build(String secPemFile) throws LuyuSDKException {
        byte[] secKey = ECSecKeyPem.read(secPemFile);
        byte[] pubKey = EcdsaSecp256k1WithSHA256.secKey2PubKey(secKey);
        LuyuSign signer = new ECDSALuyuSign();
        LuyuAccount account = LuyuAccount.build(signer, pubKey, secKey);
        return account;
    }

    public static LuyuAccount build() {
        SignatureAlgorithm alg = SignAlgManager.getAlgorithm(EcdsaSecp256k1WithSHA256.TYPE);
        Map.Entry<byte[], byte[]> keyPair = alg.generateKeyPair();

        byte[] secKey = keyPair.getValue();
        byte[] pubKey = keyPair.getKey();
        LuyuSign signer = new ECDSALuyuSign();
        LuyuAccount account = LuyuAccount.build(signer, pubKey, secKey);
        return account;
    }
}
