package link.luyu.sdk.utils;

import java.security.SecureRandom;

public class NonceUtls {
    private static final SecureRandom rand = new SecureRandom();

    public static synchronized long newNonce() {
        long n = rand.nextLong();
        if (n < 0) {
            n &= Long.MAX_VALUE;
        }
        return n;
    }
}
