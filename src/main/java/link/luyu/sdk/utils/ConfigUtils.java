package link.luyu.sdk.utils;

import com.moandjiezana.toml.Toml;
import link.luyu.sdk.exception.ErrorCode;
import link.luyu.sdk.exception.LuyuSDKException;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

public class ConfigUtils {

    public static Toml getToml(String fileName) throws LuyuSDKException {
        try {
            PathMatchingResourcePatternResolver resolver =
                    new PathMatchingResourcePatternResolver();
            return new Toml().read(resolver.getResource(fileName).getInputStream());
        } catch (Exception e) {
            throw new LuyuSDKException(
                    ErrorCode.INTERNAL_ERROR,
                    "Something wrong with parsing " + fileName + ": " + e);
        }
    }
}
