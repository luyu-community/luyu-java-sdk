package link.luyu.sdk.caller.extend;

import link.luyu.protocol.application.RemoteCall;
import link.luyu.protocol.network.Resource;
import link.luyu.sdk.caller.RemoteCallImpl;
import link.luyu.sdk.rpc.service.RPCService;

public class ListResourcesExt {

    public static class Request {
        public boolean ignoreRemote;
    }

    public static class Response {
        public int total;
        public Resource[] resources;
    }

    public static RemoteCall<Response> listResources(RPCService rpcService, boolean ignoreRemote) {
        Request request = new Request();
        request.ignoreRemote = ignoreRemote;

        return new RemoteCallImpl<Response, Object>(
                rpcService, "POST", "/sys/listResources", Response.class, request);
    }
}
