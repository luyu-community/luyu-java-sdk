package link.luyu.sdk.caller;

import link.luyu.protocol.application.RemoteCall;
import link.luyu.protocol.application.SDK;
import link.luyu.protocol.network.Block;
import link.luyu.protocol.network.CallRequest;
import link.luyu.protocol.network.CallResponse;
import link.luyu.protocol.network.LuyuAccount;
import link.luyu.protocol.network.LuyuSignData;
import link.luyu.protocol.network.Receipt;
import link.luyu.protocol.network.Resource;
import link.luyu.protocol.network.Transaction;
import link.luyu.sdk.rpc.service.RPCService;
import link.luyu.sdk.utils.NonceUtls;

public class LuyuSDK extends ExtendSDK implements SDK {
    private RPCService rpcService;
    private LuyuAccount account;

    public LuyuSDK(RPCService rpcService, LuyuAccount account) {
        super(rpcService, account);
        this.rpcService = rpcService;
        this.account = account;
    }

    @Override
    public RemoteCall<Receipt> sendTransaction(Transaction tx) {
        // set nonce
        if (tx.getNonce() == 0) {
            tx.setNonce(NonceUtls.newNonce());
        }

        if (tx.getArgs() == null) {
            tx.setArgs(new String[0]);
        }

        // set sender
        tx.setSender(account.getIdentity());

        // sign
        LuyuSignData signData = new LuyuSignData(tx);
        byte[] luyuSign = account.sign(signData);
        tx.setLuyuSign(luyuSign);

        // send
        return new RemoteCallImpl<Receipt, Transaction>(
                rpcService, "POST", "/resource/sendTransaction", Receipt.class, tx);
    }

    @Override
    public RemoteCall<CallResponse> call(CallRequest request) {
        // set nonce
        if (request.getNonce() == 0) {
            request.setNonce(NonceUtls.newNonce());
        }

        if (request.getArgs() == null) {
            request.setArgs(new String[0]);
        }

        // set sender
        request.setSender(account.getIdentity());

        // sign
        LuyuSignData signData = new LuyuSignData(request);
        byte[] luyuSign = account.sign(signData);
        request.setLuyuSign(luyuSign);

        // send
        return new RemoteCallImpl<CallResponse, CallRequest>(
                rpcService, "POST", "/resource/call", CallResponse.class, request);
    }

    @Override
    public RemoteCall<Receipt> getTransactionReceipt(String chainPath, String txHash) {
        return new RemoteCallImpl<Receipt, String>(
                rpcService,
                "POST",
                "/resource/getTransactionReceipt?chainPath=" + chainPath,
                Receipt.class,
                txHash);
    }

    @Override
    public RemoteCall<Block> getBlockByHash(String chainPath, String blockHash) {
        return new RemoteCallImpl<Block, String>(
                rpcService,
                "POST",
                "/resource/getBlockByHash?chainPath=" + chainPath,
                Block.class,
                blockHash);
    }

    @Override
    public RemoteCall<Block> getBlockByNumber(String chainPath, long blockNumber) {
        return new RemoteCallImpl<Block, Long>(
                rpcService,
                "POST",
                "/resource/getBlockByNumber?chainPath=" + chainPath,
                Block.class,
                new Long(blockNumber));
    }

    @Override
    public RemoteCall<Long> getBlockNumber(String chainPath) {
        return new RemoteCallImpl<Long, Object>(
                rpcService,
                "POST",
                "/resource/getBlockNumber?chainPath=" + chainPath,
                Long.class,
                new Object() {});
    }

    @Override
    public RemoteCall<Resource[]> listResources(String chainPath) {
        return new RemoteCallImpl<Resource[], Object>(
                rpcService,
                "POST",
                "/resource/listResources?chainPath=" + chainPath,
                Resource[].class,
                new Object() {});
    }
}
