package link.luyu.sdk.caller;

import link.luyu.protocol.application.RemoteCall;
import link.luyu.protocol.network.LuyuAccount;
import link.luyu.sdk.caller.extend.ListResourcesExt;
import link.luyu.sdk.rpc.service.RPCService;

public class ExtendSDK {
    private RPCService rpcService;
    private LuyuAccount account;

    public ExtendSDK(RPCService rpcService, LuyuAccount account) {
        this.rpcService = rpcService;
        this.account = account;
    }

    /**
     * @param ignoreRemote ignore remote resources, just return resources belongs to a router
     * @return All resources in this router
     */
    public RemoteCall<ListResourcesExt.Response> listResources(boolean ignoreRemote) {
        return ListResourcesExt.listResources(rpcService, ignoreRemote);
    }
}
