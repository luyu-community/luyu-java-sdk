package link.luyu.sdk.caller;

import com.fasterxml.jackson.databind.ObjectMapper;
import link.luyu.protocol.common.STATUS;
import link.luyu.sdk.exception.ErrorCode;
import link.luyu.sdk.exception.LuyuSDKException;
import link.luyu.sdk.rpc.service.RPCService;
import link.luyu.sdk.rpc.service.Request;
import link.luyu.sdk.rpc.service.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteCallImpl<T, R> implements link.luyu.protocol.application.RemoteCall<T> {
    private static Logger logger = LoggerFactory.getLogger(RemoteCallImpl.class);
    private static ObjectMapper objectMapper = new ObjectMapper();

    private RPCService rpcService;
    private String httpMethod;
    private String uri;
    private Object request;
    private Class<T> responseType;

    public RemoteCallImpl(
            RPCService rpcService,
            String httpMethod,
            String uri,
            Class<T> responseType,
            R request) {
        this.rpcService = rpcService;
        this.httpMethod = httpMethod;
        this.uri = uri;
        this.responseType = responseType;
        this.request = request;
    }

    @Override
    public T send() {
        try {
            Request<R> rpcRequest = new Request(request);
            Response<T> response = rpcService.send(httpMethod, uri, rpcRequest);
            if (response.getErrorCode() != STATUS.OK) {
                logger.error(
                        "RemoteCall(sync) response error, status:{}, message:{}",
                        response.getErrorCode(),
                        response.getMessage());
                return null;
            } else {
                String content = objectMapper.writeValueAsString(response.getData());
                T data = objectMapper.readValue(content, responseType);
                return data;
            }
        } catch (Exception e) {
            logger.error("RemoteCall(sync) exception ", e);
            return null;
        }
    }

    @Override
    public void asyncSend(Callback<T> callback) {
        Request<R> rpcRequest = new Request(request);
        rpcService.asyncSend(
                httpMethod,
                uri,
                rpcRequest,
                new link.luyu.sdk.rpc.service.Callback<T>() {
                    @Override
                    public void onSuccess(Response<T> response) {
                        try {
                            String content = objectMapper.writeValueAsString(response.getData());
                            T data = objectMapper.readValue(content, responseType);

                            callback.onResponse(
                                    response.getErrorCode(), response.getMessage(), data);
                        } catch (Exception e) {
                            onFailed(
                                    new LuyuSDKException(
                                            ErrorCode.SERIALIZATION_EXCEPTION, e.getMessage()));
                        }
                    }

                    @Override
                    public void onFailed(LuyuSDKException e) {
                        logger.error("RemoteCall(async) exception ", e);
                        callback.onResponse(STATUS.INTERNAL_ERROR, e.getMessage(), null);
                    }
                });
    }
}
