package link.luyu.sdk.caller;

import link.luyu.protocol.network.LuyuAccount;
import link.luyu.sdk.exception.LuyuSDKException;
import link.luyu.sdk.rpc.service.AsyncHttpClientService;
import link.luyu.sdk.rpc.service.RPCService;

public class SDKFactory {
    private SDKFactory() {}

    public static LuyuSDK build(RPCService rpcService, LuyuAccount account)
            throws LuyuSDKException {
        rpcService.init();
        return new LuyuSDK(rpcService, account);
    }

    public static LuyuSDK build(LuyuAccount account) throws LuyuSDKException {
        RPCService rpcService = new AsyncHttpClientService();
        rpcService.init();
        return new LuyuSDK(rpcService, account);
    }
}
