package link.luyu.sdk.common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.Security;
import java.security.spec.ECFieldFp;
import java.security.spec.ECPoint;
import java.security.spec.ECPrivateKeySpec;
import java.security.spec.EllipticCurve;
import java.security.spec.PKCS8EncodedKeySpec;
import link.luyu.sdk.exception.ErrorCode;
import link.luyu.sdk.exception.LuyuSDKException;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.jce.spec.IESParameterSpec;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

public class ECSecKeyPem {
    public static final class ECCParams {

        // ECDSA secp256k1 algorithm constants
        public static final BigInteger POINTG_PRE =
                new BigInteger(
                        "79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798", 16);
        public static final BigInteger POINTG_POST =
                new BigInteger(
                        "483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8", 16);
        public static final BigInteger FACTOR_N =
                new BigInteger(
                        "fffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141", 16);
        public static final BigInteger FIELD_P =
                new BigInteger(
                        "fffffffffffffffffffffffffffffffffffffffffffffffffffffffefffffc2f", 16);
        public static final EllipticCurve ellipticCurve =
                new EllipticCurve(new ECFieldFp(FIELD_P), new BigInteger("0"), new BigInteger("7"));
        public static final ECPoint pointG = new ECPoint(POINTG_PRE, POINTG_POST);
        public static final ECNamedCurveSpec ecNamedCurveSpec =
                new ECNamedCurveSpec("secp256k1", ellipticCurve, pointG, FACTOR_N);

        public static final IESParameterSpec IES_PARAMS = new IESParameterSpec(null, null, 64);
    }

    private static Logger logger = LoggerFactory.getLogger(ECSecKeyPem.class);

    public static byte[] read(String pemFilePath) throws LuyuSDKException {
        Security.addProvider(new BouncyCastleProvider());
        org.bouncycastle.util.io.pem.PemReader pemReader = null;
        try {
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource pemResources = resolver.getResource(pemFilePath);

            pemReader =
                    new org.bouncycastle.util.io.pem.PemReader(
                            new InputStreamReader(pemResources.getInputStream()));

            PemObject pem = pemReader.readPemObject();
            if (pem == null) {
                throw new LuyuSDKException(
                        ErrorCode.INTERNAL_ERROR,
                        "The file " + pemFilePath + " does not represent a pem account.");
            }

            PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(pem.getContent());
            KeyFactory keyFacotry =
                    KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);

            BCECPrivateKey privateKey = (BCECPrivateKey) keyFacotry.generatePrivate(encodedKeySpec);

            BigInteger privateKeyValue = privateKey.getD();
            return privateKeyValue.toByteArray();

        } catch (Exception e) {
            throw new LuyuSDKException(ErrorCode.INTERNAL_ERROR, e.getMessage());
        } finally {
            try {
                if (pemReader != null) {
                    pemReader.close();
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
                throw new LuyuSDKException(ErrorCode.INTERNAL_ERROR, e.getMessage());
            }
        }
    }

    public static byte[] readContent(byte[] content) throws LuyuSDKException {
        try {
            PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(content);
            KeyFactory keyFacotry =
                    KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);

            BCECPrivateKey privateKey = (BCECPrivateKey) keyFacotry.generatePrivate(encodedKeySpec);

            BigInteger privateKeyValue = privateKey.getD();
            return privateKeyValue.toByteArray();
        } catch (Exception e) {
            throw new LuyuSDKException(ErrorCode.INTERNAL_ERROR, e.getMessage());
        }
    }

    public static void write(String toPemFilePath, byte[] secKey) throws IOException {
        byte[] encodedSecKey = encodeSecKey(secKey);
        writeEncoded(toPemFilePath, encodedSecKey);
    }

    public static void writeEncoded(String toPemFilePath, byte[] encodedSecKey) throws IOException {

        File file = new File(toPemFilePath);
        if (!file.createNewFile()) {
            throw new IOException("Key file exists!" + toPemFilePath);
        }

        PemWriter pemWriter = new PemWriter(new FileWriter(file));
        try {
            pemWriter.writeObject(new PemObject("PRIVATE KEY", encodedSecKey));
        } finally {
            pemWriter.close();
        }
    }

    private static byte[] encodeSecKey(byte[] secKey) {
        BigInteger privateKey = new BigInteger(1, secKey);
        // Handle secret key
        ECPrivateKeySpec secretKeySpec =
                new ECPrivateKeySpec(privateKey, ECCParams.ecNamedCurveSpec);
        BCECPrivateKey bcecPrivateKey =
                new BCECPrivateKey("ECDSA", secretKeySpec, BouncyCastleProvider.CONFIGURATION);
        return bcecPrivateKey.getEncoded();
    }
}
